<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

function __construct() {
parent::__construct();
	$this->load->helper('url');
	$this->load->model("Student_model");
	$this->load->library('session');
    $this->load->helper('form');
    $this->load->helper('url');
    $this->load->database();
    $this->load->library('form_validation');
}
	
	public function index()
	{
		
		$this->load->view('index');
	}
	public function studentAdd()
	{
		   $this->load->view('admin/header.php');
           $this->load->view('student/add'); 
		   $this->load->view('admin/footer.php');
		   $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

			//Validating Name Field
			$this->form_validation->set_rules('dname', 'Username', 'required|min_length[5]|max_length[15]');

			//Validating Email Field
			$this->form_validation->set_rules('demail', 'Email', 'required|valid_email');

		 
	}
	
	public function studentInsert()
	{
		
		$data = array(  
				'student_first_name'  => $this->input->post('first_name'),  
				'student_last_name'  => $this->input->post('last_name'),  
				'student_father_name'  => $this->input->post('fathers_name'),  
				'student_mother_name'  => $this->input->post('mothers_name'),  
				'student_previous_class'  => $this->input->post('previous_class'),  
				'student__previous_school_name'  => $this->input->post('previous_school'),  
				'student_address'  => $this->input->post('address'),  
				'student_mobile'  => $this->input->post('mobile'), 
				'student_dob'   => date('Y-m-d', strtotime($this->input->post('birthdate'))), 
				'student_gender' =>$this->input->post('gender')
				);  
				
				//Transfering data to Model
				$this->Student_model->studentDataInsert($data);
				//Loading View
				$this->load->view('admin/header.php');
				$this->load->view('student/manage');
				$this->load->view('admin/footer.php');
	}
	
	public function studentManage()
	{
				$this->load->view('admin/header.php');
				$this->load->view('student/manage');
				$this->load->view('admin/footer.php');
		
	}
	
	public function studentListing()
	{
		$results = $this->Student_model->get_cd_list();
        echo json_encode($results);
		
	}
	
}
