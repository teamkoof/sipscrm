<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Visitor extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('encryption');
        $this->load->model("Visitor_model");
        $this->load->library('session');
        $this->load->database();
        $this->load->library('table');
    }

    public function visitorManage() {
        $this->load->view('admin/header.php');
        $this->load->view('visitor/manage');
        $this->load->view('admin/footer.php');
    }

    public function visitorAdd() {

        try {

            $this->form_validation->set_rules('name', 'Name', 'required|min_length[2]|max_length[100]');
            $this->form_validation->set_rules('phone', 'Phone', 'required|min_length[2]|max_length[100]');
            $this->form_validation->set_rules('coming_from', 'Coming From', 'required|min_length[2]|max_length[100]');
            $this->form_validation->set_rules('phone', 'Phone', 'required|min_length[2]|max_length[100]');
            if ($this->form_validation->run() == TRUE) {
                $name = $this->security->xss_clean($this->input->post('name'));
                $email = $this->security->xss_clean($this->input->post('email_id'));
                $phone = $this->security->xss_clean($this->input->post('phone'));
                $comingFrom = $this->security->xss_clean($this->input->post('coming_from'));
                $userType = $this->security->xss_clean($this->input->post('usertypeID'));
                $toMeet = $this->security->xss_clean($this->input->post('to_meet'));
                $representing = $this->security->xss_clean($this->input->post('representing'));
                
                $data = array(
                    'visitor_name' => $name,
                    'visitor_email' => $email,
                    'visitor_phone' => $phone,
                    'visitor_meet_person_type' => $userType,
                    'visitor_to_meet' => $toMeet,
                    'visitor_representing' => $representing,
                    'visitor_coming_from' => $comingFrom,
                    'visitor_in_date_time'=>Date('Y-m-d H:i:s'),
                    'visitor_logged_in' => 'Y'
                );
                
                $str = $this->db->insert('sips_visitor_info', $data);
                $this->session->set_flashdata('success', 'Visitor Successfully Logged in');
                redirect('Visitor/visitorManage');
            }
            $this->load->view('admin/header.php');
            $this->load->view('visitor/add');
            $this->load->view('admin/footer.php');
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    public function visitorListing() {
        $results = $this->Visitor_model->get_cd_list();
        echo json_encode($results);
    }

}
