<script type="text/javascript">
            $(document).ready(function() {
              $('#birthdate').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_4"
              }, function(start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
              });
            });
          </script>
		  
<div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3>Add a Student</h3>
            </div>
          </div>
          <div class="clearfix"></div>
		<div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Student Details</h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <br />
				  <?php 
        $attributes = array("class" => "form-horizontal form-label-left", "id" => "studentform", "name" => "studentform","method"=>"POST");
        echo form_open("dashboard/studentInsert", $attributes);?>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">First Name <span class="required" style="color:red">*</span>
                      </label>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <input type="text" id="first_name" name="first_name" required="required" class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Last Name <span class="required" style="color:red">*</span>
                      </label>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <input type="text" id="last_name" name="last_name" required="required" class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="gender">Gender<span class="required" style="color:red">*</span></label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <div id="gender" class="btn-group btn-group-sm" data-toggle="buttons">
                          <label class="btn btn-default" data-toggle-class="btn-info" data-toggle-passive-class="btn-success">
                            <input type="radio" id= "gender" name="gender" value="male"> &nbsp; Male &nbsp;
                          </label>
                          <label class="btn btn-default" data-toggle-class="btn-info" data-toggle-passive-class="btn-success">
                            <input type="radio" id="gender" name="gender" value="female"> Female
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="birthdate">Date Of Birth<span class="required" style="color:red">*</span></label>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <input id="birthdate" name="birthdate" class="date-picker form-control col-md-7 col-xs-12" required="required" type="text">
                      </div>
                    </div>
					 <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="mothers-name">Mother's Name <span class="required" style="color:red">*</span>
                      </label>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <input type="text" id="mothers_name" name="mothers_name" required="required" class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>
					 <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="fathers-name">Father's Name <span class="required" style="color:red">*</span>
                      </label>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <input type="text" id="fathers_name" name="fathers_name" required="required" class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>
					   <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="previous_class">Previous Class <span class="required" style="color:red">*</span>
                      </label>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <input type="text" id="previous_class" name="previous_class" required="required" class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="previous_school">Previous School <span class="required" style="color:red">*</span>
                      </label>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <input type="text" id="previous_school" name="previous_school" required="required" class="form-control col-md-7 col-xs-12">
                      </div>
					  </div>
					  <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="mobile">Mobile<span class="required" style="color:red">*</span>
                      </label>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <input type="text" id="mobile" name="mobile" required="required" class="form-control col-md-7 col-xs-12">
                      </div>
					  </div>
					   <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">Address<span class="required" style="color:red">*</span>
                      </label>
                      <div class="col-md-4 col-sm-4 col-xs-12">
					  <textarea class="form-control col-md-7 col-xs-12" rows="2" id="address" name="address"></textarea>
                      </div>
					  </div>
                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success" style="margin-right:50px;" >Submit</button>
						<button class="btn btn-primary" onclick="location.href='<?php echo base_url();?>Dashboard/studentManage'">Cancel</button>
                      </div>
                    </div>

                  </form>
                </div>
              </div>
            </div>
          </div>
</div>
          
