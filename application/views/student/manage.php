 <script type= 'text/javascript'>
            $(document).ready(function () {
                $('#cd-grid').DataTable({
                    "processing": true,
                    "serverSide": true,
                    "ajax": "http://localhost/sipscrm/Dashboard/studentListing"
                });
            });
        </script>
 <div class="right_col" role="main">
        <div class="">

          <div class="page-title">
            <div class="title_left">
              <h3>
                    Manage Student
                </h3>
            </div>

            <div class="title_right">
              <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
				<a href="studentAdd" class="btn btn-info btn-round-md btn-md" style="border-radius: 20px;  width:150px;" role="button">Add Student</a>
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
		   <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
			   <div class="x_content">
                  <table id="cd-grid" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>Sr No</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Date Of Birth</th>
                        <th>Mobile</th>
                      </tr>
                    </thead>
        </table>
		  </div>
		  </div>
		  </div>
		  </div>