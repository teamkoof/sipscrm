<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script> 
<script>
    $(document).ready(function () {
        $("#visitorform").validate({
            rules: {
                name: {
                    required: true,
                    minlength: 3,
                    maxlength: 40
                },
                email_id: {
                    email: true
                },
                phone: {
                    required: true,
                    number: true,
                    minlength: 10,
                    maxlength: 12
                },
                coming_from: {
                    required: true,
                     minlength: 2,
                    maxlength: 20
                },
                usertypeID:{
                    required: true
                },
                to_meet:{
                    required: true
                }
                
            },
            messages: {
                name: {
                    required: "Please enter visitor name",
                    minlength: $.format("Keep typing, at least {0} characters required!"),
                    maxlength: $.format("Whoa! Maximum {0} characters allowed!")
                },
                email_id: {
                    email: "Please enter a valid email address"
                },
                phone: {
                    required: "Please enter visitor mobile number",
                    number: "Please, enter number only",
                    minlength: $.format("Keep typing, at least {0} characters required!"),
                    maxlength: $.format("Whoa! Maximum {0} characters allowed!")
                },
                coming_from:{
                    required: "Please, enter from where visitor is coming",
                    minlength: $.format("Keep typing, at least {0} characters required!"),
                    maxlength: $.format("Whoa! Maximum {0} characters allowed!")
                },
                usertypeID:{
                    required: "Please, select the whom the visitor want to meet"
                },
                to_meet:{
                    required: "Please, Enter name whom visitor want to meet"
                }
                

            }
        });
    });
</script>
<style>
  form  label.error{
        width: 100%;
        color: red;
        font-style: bold;
        display:inline;
    }
</style>
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.html">Home</a><i class="fa fa-angle-right"></i>Add Visitor</li>
                </ol>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Visitor Details</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />

                        <div class="box-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="col-sm-7">
                                        <?php
                                        $attributes = array("class" => "form-horizontal form-label-left visitorform", "id" => "visitorform", "name" => "visitorform", "method" => "POST");
                                        echo form_open("visitor/visitorAdd", $attributes);
                                        ?>
                                        <div class="form-group">
                                            <label for="name" class="col-sm-3 control-label">
                                                Name<span style="color: red;">*</span></label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control input-sm" id="name" name="name" value="">
                                                <span class="text-danger" id="error_name"></span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="email_id" class="col-sm-3 control-label">
                                                Email</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control input-sm" id="email_id" name="email_id" value="">
                                                <span class="text-danger" id="error_email_id"></span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="phone" class="col-sm-3 control-label">
                                                Phone<span style="color: red;">*</span></label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control input-sm" id="phone" name="phone" value="">
                                                <span class="text-danger" id="error_phone"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="coming_from" class="col-sm-3 control-label">
                                                Coming from <span style="color: red;">*</span></label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control input-sm" id="coming_from" name="coming_from" value="">
                                                <span class="text-danger" id="error_coming_from"></span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="usertypeID" class="col-sm-3 control-label">
                                                To meet user type <span style="color: red;">*</span> </label>
                                            <div class="col-sm-8">
                                                <select name="usertypeID" id="usertypeID" class="form-control">
                                                    <option value="">Select Role</option>
                                                    <option value="Moderator">Moderator</option>
                                                    <option value="Receptionist">Receptionist</option>
                                                    <option value="Librarian">Librarian</option>
                                                    <option value="Accountant">Accountant</option>
                                                    <option value="Parents">Parents</option>
                                                    <option value="Student">Student</option>
                                                    <option value="Teacher">Teacher</option>
                                                    <option value="Administrator">Administrator</option>
                                                </select>
                                                <span class="text-danger" id="error_to_meet_personID"></span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="to_meet" class="col-sm-3 control-label">
                                                To meet<span style="color: red;">*</span></label>

                                            <div class="col-sm-8">
                                                <input type="text" class="form-control input-sm" id="to_meet" name="to_meet" value="">
                                                <span class="text-danger" id="error_to_meet"></span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="representing" class="col-sm-3 control-label">
                                                Representing<span style="color: red;">*</span></label>
                                            <div class="col-sm-8">
                                                <select class="form-control input-sm" name="representing" id="representing">
                                                    <option value="vendor">Vendor</option>
                                                    <option value="friend">Friend</option>
                                                    <option value="family">Family</option>
                                                    <option value="interview">Interview</option>
                                                    <option value="meeting">Meeting</option>
                                                    <option value="other">Other</option>
                                                </select>
                                            </div>
                                            <span class="text-danger" id="error_representing"></span>
                                        </div>
                                        <input id="mydata" type="hidden" name="mydata" value="">

                                        <div class="form-group">
                                            <div class="col-sm-offset-3 col-sm-8">
                                                <button type="submit" class="btn btn-success" style="margin-right:50px;" >Create Pass</button>

                                                <input class="btn btn-danger" type="button" value="Cancel">
                                            </div>
                                        </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('#birthdate').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_4"
        }, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });
    });
</script>

