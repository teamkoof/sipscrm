<script type= 'text/javascript'>
    $(document).ready(function () {
        $('#cd-grid').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "http://localhost/sipscrm/Visitor/visitorListing"
        });
    });
</script>
<style>
    .sorting, .sorting_asc, .sorting_desc {
        background : none;
    }
    .alert{
        padding: 6px;   
        margin-bottom: 0px;
    }
</style>
<div class="right_col" role="main">
    <div class="">

        <div class="page-title">
            <div class="title_left">
                <h3>
                    Manage Visitor
                </h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    <div class="input-group">
                        <a href="visitorAdd" class="btn btn-info btn-round-md btn-md" style="border-radius: 20px;  width:150px;" role="button">Add Visitor</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <?php if ($this->session->flashdata('success') != "") { ?>
                <div class="seccessMsg form-inline pull-left">
                    <div class="alert alert-success">
                        <?php echo $this->session->flashdata('success'); ?>
                    </div>
                </div>
            <?php } ?>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                        <table id="cd-grid" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Sr No</th>
                                    <th>Visitor Name</th>
                                    <th>Email</th>
                                    <th>Mobile</th>
                                    <th>To Meet</th>
                                    <th>Coming From</th>
                                    <th>Updates</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>